import React from 'react';
import { View, Text, StyleSheet, FlatList, Image } from 'react-native';

const ImageScreen = ({ title, imageSource, score }) => {
   
    
    return <View>
        <Image source={imageSource} />
        <Text>Forest</Text>
        <Text>Image {title} score {score}</Text>
    </View>
};

const styles = StyleSheet.create({});

export default ImageScreen;