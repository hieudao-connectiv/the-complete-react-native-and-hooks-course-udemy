import React from "react";
import { Text, StyleSheet, View, Button, Alert } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const HomeScreen = ({ navigation }) => {
  // console.log(props.navigation.navigate('Components'))

  // const greeting = 'Bye there!'        // Bye there
  // const greeting = [12,321]            // 12321 
  // const greeting = ['hieu', 'dao']     // hieudao
  // const greeting = { color: 'red' }    // we can't show a JS object inside of <Text />
  const myName = <Text>Dao Trung Hieu</Text>

  // return <View>
  //   <Text style={styles.text}>Hi There!</Text>
  //   <Text>{greeting}</Text>
  // </View>

  // Or
  return (
    <View>
      <Text style={styles.text}>Getting started with react native</Text>
      <Button 
        color='pink'
        title="Go to Components Demo"
        onPress={() => navigation.navigate('Components')}
      />
      <Button 
        // color='pink'
        title="Go to List Demo"
        onPress={() => navigation.navigate('List')}
      />
      <Button 
        color='pink'
        title="Go to ImageScreen Demo"
        onPress={() => navigation.navigate('Image')}
      />
      <Button 
        // color='pink'
        title="Go to CounterScreen Demo"
        onPress={() => navigation.navigate('Counter')}
      />
      <Button 
        color='pink'
        title="Go to ColorScreen Demo"
        onPress={() => navigation.navigate('Color')}
      />
      <Button 
        // color='pink'
        title="Go to SquareScreen Demo"
        onPress={() => navigation.navigate('Square')}
      />
      <Button 
        color='pink'
        title="Go to InputScreen Demo"
        onPress={() => navigation.navigate('Input')}
      />
      {/* <TouchableOpacity
        onPress={() => props.navigation.navigate('List')}
      >
        <Text>Go to List Demo</Text>
      </TouchableOpacity> */}
    </View>
  )
};

const styles = StyleSheet.create({
  text: {
    fontSize: 45,
  },
  myName: {
    fontSize: 20,
  }
});

export default HomeScreen;
