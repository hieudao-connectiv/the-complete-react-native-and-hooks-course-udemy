import React, { useState, useReducer } from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';

const reducer = (state, action) => {
    // state === { counter: 0 }
    // action === { type: 'increase' || 'decrease', payload: 1}

    switch (action.type) {
        case 'increase': 
            return {...state, counter: state.counter + action.payload }
        case 'decrease': 
            return {...state, counter: state.counter - action.payload }
        default: 
            return state
    }
}

const ComponentsScreen = () => {
    // const [ counter, setCounter ] = useState(0)
    const [ state, dispatch ] = useReducer(reducer, { counter: 0 })

    return <View>
        <Button
            title="Increase" 
            onPress={() => {
                // setCounter(counter + 1)
                // console.log(counter)
                dispatch({ type: 'increase', payload: 1})
            }}
        />
        <Button
            title="Decrease" 
            onPress={() => {
                // setCounter(counter - 1)
                // console.log(counter)
                dispatch({ type: 'decrease', payload: 1})
            }}
        />
        <Text>Current Count: {state.counter}</Text>

    </View>
}

const styles = StyleSheet.create({})

export default ComponentsScreen;