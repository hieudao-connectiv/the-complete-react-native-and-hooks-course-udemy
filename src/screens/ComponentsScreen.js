import React from 'react';
import { Text, StyleSheet } from 'react-native';

const ComponentsScreen = () => {
    // This code then passes into that React Native Bundler that's running inside of our terminal. That bundler is using a tool called Bable to turn this JSX into some plain JS code
    return <Text style={styles.textStyle}>This is my Components screen</Text>
}

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 30
    }
})

export default ComponentsScreen;