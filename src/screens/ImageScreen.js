import React from 'react';
import { View, Text, StyleSheet, FlatList, Image } from 'react-native';
import ImageDetail from '../components/ImageDetail';

const ImageScreen = () => {
    const datas = [
        { img: '../../assets/forest.jpg', title: 'Dao', score: '6'},
        { img: '../../assets/forest.jpg', title: 'Trung', score: '9'},
        { img: 'ht../../assets/forest.jpg', title: 'Hieu', score: '10'},
    ]
    
    // return <View>
    //     <ImageDetail 
    //         title="Forest" 
    //         imageSource={require('../../assets/forest.jpg')}
    //         score='7' 
    //     />
    //     {/* <ImageDetail 
    //         title="Beach" 
    //         imageSource={require('../../assets/beach.jpg')}
    //         score='9' 
    //     />
    //     <ImageDetail 
    //         title="Mountain" 
    //         imageSource={require('../../assets/mountain.jpg')}
    //         score='10' 
    //     /> */}
    // </View>
    return <View>
        <FlatList
            data={datas}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={data => data.content}
            renderItem={({ item }) => {
                console.log(item)
                const newimg = item.img
                return (
                    <ImageDetail 
                        title={item.title} 
                        // imageSource={require({newimg})}
                        score={item.score}
                    />
                )
            }}
        />
    </View>
};

const styles = StyleSheet.create({});

export default ImageScreen;