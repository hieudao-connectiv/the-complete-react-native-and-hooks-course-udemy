import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';

const ListScreen = () => {
    const friends = [
        {name: 'Friend #1', age: '20'},
        {name: 'Friend #2', age: '18'},
        {name: 'Friend #3', age: '10'},
        {name: 'Friend #4', age: '25'},
        {name: 'Friend #5', age: '23'},
        {name: 'Friend #6', age: '20'},
        {name: 'Friend #7', age: '12'},
        {name: 'Friend #8', age: '20'},
        {name: 'Friend #9', age: '32'},
        {name: 'Friend #10', age: '30'},
    ]
    return (
        <FlatList 
            // horizontal
            // showsHorizontalScrollIndicator={false}
            // we're using that name property as our key
            keyExtractor={friend => friend.name}
            data={friends} 

            // renderItem={(element) => {
                // element = {item: {name: 'Friend #1' }, index: 0}

            renderItem={({ item }) => {
            //  item: {name: 'Friend #1' }
                return <Text style={styles.textStyle}>
                    {item.name} - Age {item.age}
                </Text>    
            }} 
        />
    )
}

const styles = StyleSheet.create({});

export default ListScreen;