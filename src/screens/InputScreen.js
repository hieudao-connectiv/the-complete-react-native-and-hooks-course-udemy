import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';

const InputScreen = () => {
    const [ name, setName ] = useState('')

    return <View>
        <Text>Enter Name:</Text>
        <TextInput 
            style={ styles.inputText } 
            autoCapitalize='none'
            autoCorrect={false}
            autoFocus={true}
            onChangeText={(newValue) => setName(newValue)} 
            value={name}
        />
        <Text>Your name is: {name}</Text>
        {name.length < 5 ? <Text>Your name is must be longer than 5 characters</Text> : null}
    </View>
}

const styles = StyleSheet.create({
    inputText: {
        marginLeft: 5,
        marginRight: 5,
        borderColor: 'blue',
        borderWidth: 1,
        paddingLeft: 5
    }
})

export default InputScreen;