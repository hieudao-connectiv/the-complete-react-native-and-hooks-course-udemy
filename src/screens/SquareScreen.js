import React, { useReducer } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ColorCounter from '../components/ColorCounter';

const COLOR_INCREASE = 15

const reducer = (state, action) => {
    // state === { red: number, green: number, blue: number };
    // action === { colorToChange: 'red' || 'green' || 'blue', amount: 15 || -15 }
    // action === { type: 'change_red' || 'change_green' || 'change_blue', payload: 15 || -15 }

    switch (action.colorToChange) {
        case 'red':

            // need return 'state' to render output otherwise an error "undefined is not an object (evaluating 'state.red')"
            return (state.red + action.amount > 255 || state.red + action.amount < 0) ? state : {...state, red: state.red + action.amount}
        case 'green':
            return (state.green + action.amount > 255 || state.green + action.amount < 0) 
            ? state : {...state, green: state.green + action.amount}
        case 'blue':
            return (state.blue + action.amount > 255 || state.blue + action.amount < 0) 
            ? state : {...state, blue: state.blue + action.amount}
        default:
            return state
    }
}

const SquareScreen = () => {

    // we define our reducer function inside of square screen. that's called States it'll be very quickly confused with this other declaration of state over here. Now technically this would not result in an error but it's gonna make life for you really confusing because you're going to have essentially two different variables called state floating around the inside of this one component.
    // const reducer = (state, action) => {}

    const [ state, dispatch ] = useReducer(reducer, { red: 0, green: 0, blue: 0})
    console.log(state)

    // const [ red, setRed ] = useState(0)
    // const [ green, setGreen ] = useState(0)
    // const [ blue, setBlue ] = useState(0)
    
    // console.log(`Red: ${red}`)
    // console.log(`Green: ${green}`)
    // console.log(`Blue: ${blue}`)
    // console.log("--------")

    // const setColor = (color, change) => {
    //     // color === 'red'/'green'/'blue'
    //     // change === +15/-15
        
    //     switch (color) {
    //         case 'red': 
    //             if (red + change > 255 || red + change < 0) {
    //                 return
    //             } else {
    //                 setRed(red + change)
    //             }
    //             break
    //         case 'green':
    //             (green + change > 255 || green + change < 0) 
    //             ? null : setGreen(green + change)
    //             break
    //         case 'blue': 
    //             (blue + change > 255 || blue + change < 0) 
    //             ? null : setBlue(blue + change)
    //             break
    //         default: 
    //             alert(`Something was wrong with ${color}`)
    //     }
        
    // }

    return <View>
        <Text>Square Screen</Text>
        <ColorCounter 
            // onIncrease={() => setColor('red', COLOR_INCREASE)} 
            // onDecrease={() => setColor('red', -COLOR_INCREASE)}  
            onIncrease={() => dispatch({ colorToChange: 'red', amount: COLOR_INCREASE})}  
            onDecrease={() => dispatch({ colorToChange: 'red', amount: -1 * COLOR_INCREASE})}  
            color="Red" 
        />
        <ColorCounter 
            // onIncrease={() => setColor('green', COLOR_INCREASE)}         
            // onDecrease={() => setColor('green', -COLOR_INCREASE)}  
            onIncrease={() => dispatch({ colorToChange: 'green', amount: COLOR_INCREASE})}
            onDecrease={() => dispatch({ colorToChange: 'green', amount: -1 * COLOR_INCREASE})}  
            color="Green" 
        />
        <ColorCounter 
            // onIncrease={() => setColor('blue', COLOR_INCREASE)} 
            // onDecrease={() => setColor('blue', -COLOR_INCREASE)} 
            onIncrease={() => dispatch({ colorToChange: 'blue', amount: COLOR_INCREASE})} 
            onDecrease={() => dispatch({ colorToChange: 'blue', amount: -1 * COLOR_INCREASE})}  
            color="Blue" 
        />
        <View style={{ height: 150, width: 150, marginTop: 12, backgroundColor:`rgb(${state.red}, ${state.green}, ${state.blue})` }} />
        <Text>{`rgb(${state.red}, ${state.green}, ${state.blue})`}</Text>

    </View>
}

const styles = StyleSheet.create({})

export default SquareScreen;